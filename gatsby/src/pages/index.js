import React from 'react';

export default function HomePage() {
  return (
    // wrap in a ghost element (react fragment short syntax)
    // https://reactjs.org/docs/fragments.html
    <>
      <p>Hello! ce faci Mr?</p>
    </>
  );
}
